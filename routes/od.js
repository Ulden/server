/**
 * Created by shushu on 4/22/17.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('OD', { title: 'OD' });
});

module.exports = router;
