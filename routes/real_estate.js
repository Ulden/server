/**
 * Created by shushu on 4/23/17.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('real_estate', { title: 'real_estate' });
});

module.exports = router;
